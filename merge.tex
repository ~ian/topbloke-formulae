\section{Merge}
\label{commit-merge}

Merge commits $L$ and $R$ using merge base $M$:
\gathbegin
 C \hasparents \{ L, R \}
\gathnext
 \patchof{C} = \patchof{L}
\gathnext
 \commitmergeof{C}{L}{M}{R}
\end{gather}
We will occasionally use $X,Y$ s.t. $\{X,Y\} = \{L,R\}$.

This can also be used for dependency re-insertion, by setting $L \in
\pn$, $R \in \pry$, $M = \baseof{R}$, provided that the Conditions are
satisfied; in particular, provided that $L \ge \baseof{R}$.

\subsection{Conditions}
\[ \eqn{ Ingredients }{
 M \le L \land M \le R
}\]
\[ \eqn{ Tip Merge }{
 L \in \py \implies
   \begin{cases}
      R \in \py : & \baseof{R} \ge \baseof{L}
              \land [\baseof{L} = M \lor \baseof{L} = \baseof{M}] \\
      R \in \pn : & M = \baseof{L} \\
      \text{otherwise} : & \false
   \end{cases}
}\]
\[ \eqn{ Base Merge }{
 L \in \pn \implies
  \big[
   R \in \pn
     \lor
   R \in \foreign
     \lor
   ( R \in \pqy \land \pq \neq \p )
  \big]
}\]
\[ \eqn{ Merge Acyclic }{
    L \in \pn
   \implies
    R \nothaspatch \p
}\]
\[ \eqn{ Removal Merge Ends }{
    X \not\haspatch \p \land
    M \haspatch \p \land
    Y \haspatch \p
  \implies
    \pendsof{Y}{\py} = \pendsof{M}{\py}
}\]
\[ \eqn{ Addition Merge Ends }{
    X \not\haspatch \p \land
    M \nothaspatch \p \land
    Y \haspatch \p
   \implies \left[
    \bigforall_{E \in \pendsof{X}{\py}} E \le Y
   \right]
}\]
\[ \eqn{ Suitable Tips }{
    \bigforall_{\p \patchisin C, \; \py \neq \patchof{L}}
    \bigexists_T
      \pendsof{J}{\py} = \{ T \}
     \land
      \forall_{E \in \pendsof{K}{\py}} T \ge E
    , \text{where} \{J,K\} = \{L,R\}
}\]
\[ \eqn{ Foreign Merge }{
    \isforeign{L} \implies \isforeign{R}
}\]

\subsection{Non-Topbloke merges}

We require both $\isforeign{L}$ and $\isforeign{R}$
(Foreign Merge, above).
I.e. not only is it forbidden to merge into a Topbloke-controlled
branch without Topbloke's assistance, it is also forbidden to
merge any Topbloke-controlled branch into any plain git branch.

Given those conditions, Tip Merge and Merge Acyclic do not apply.
By Foreign Ancestry of $L$, $\isforeign{M}$ as well.
So by Foreign Ancestry for any $A \in \{L,M,R\}$,
$\forall_{\p, D \in \py} D \not\le A$
so $\pendsof{A}{\py} = \{ \}$ and the RHS of both Merge Ends
conditions are satisifed.

So a plain git merge of non-Topbloke branches meets the conditions and
is therefore consistent with our model.

\subsection{No Replay}

By definition of \commitmergename,
$D \isin C \implies D \isin L \lor D \isin R \lor D = C$.
So, by Ingredients,
Ingredients Prevent Replay applies.  $\qed$

\subsection{Unique Base}

Need to consider only $C \in \py$, ie $L \in \py$,
and calculate $\pendsof{C}{\pn}$.  So we will consider some
putative ancestor $A \in \pn$ and see whether $A \le C$.

By Exact Ancestors for C, $A \le C \equiv A \le L \lor A \le R \lor A = C$.
But $C \in \py$ and $A \in \pn$ so $A \neq C$.
Thus $A \le C \equiv A \le L \lor A \le R$.

By Unique Base of L and Transitive Ancestors,
$A \le L \equiv A \le \baseof{L}$.

\subsubsection{For $R \in \py$:}

By Unique Base of $R$ and Transitive Ancestors,
$A \le R \equiv A \le \baseof{R}$.

But by Tip Merge condition on $\baseof{R}$,
$A \le \baseof{L} \implies A \le \baseof{R}$, so
$A \le \baseof{R} \lor A \le \baseof{L} \equiv A \le \baseof{R}$.
Thus $A \le C \equiv A \le \baseof{R}$.
That is, $\baseof{C} = \baseof{R}$.

\subsubsection{For $R \in \pn$:}

By Tip Merge condition and since $M \le R$,
$A \le \baseof{L} \implies A \le R$, so
$A \le R \lor A \le \baseof{L} \equiv A \le R$.
Thus $A \le C \equiv A \le R$.
That is, $\baseof{C} = R$.

$\qed$

\subsection{Coherence and Patch Inclusion}

$C$ satisfies
\gathbegin
  C \haspatch \p \lor C \nothaspatch \p
\gathnext
C \haspatch \p \equiv
  \stmtmergeof{L \haspatch \p}{M \haspatch \p}{R \haspatch \p}
\end{gather}
which (given Coherence of $L$,$M$,$R$) is equivalent to
$$
\begin{cases}
  L \nothaspatch \p \land R \nothaspatch \p : & C \nothaspatch \p  \\
  L \haspatch    \p \land R \haspatch    \p : & C \haspatch    \p  \\
  \text{otherwise} \land M \haspatch    \p  : & C \nothaspatch \p  \\
  \text{otherwise} \land M \nothaspatch \p  : & C \haspatch    \p
\end{cases}
$$
\proofstarts
~ Consider $D \in \py$.

\subsubsection{For $L \nothaspatch \p, R \nothaspatch \p$:}
$D \not\isin L \land D \not\isin R$.  $C \not\in \py$ (otherwise $L
\in \py$ ie $L \haspatch \p$ by Tip Own Contents for $L$).
So $D \neq C$.
Applying \commitmergename\ gives $D \not\isin C$ i.e. $C \nothaspatch \p$.
OK.

\subsubsection{For $L \haspatch \p, R \haspatch \p$:}
$D \isin L \equiv D \le L$ and $D \isin R \equiv D \le R$.
(Likewise $D \isin X \equiv D \le X$ and $D \isin Y \equiv D \le Y$.)

Consider $D = C$: $D \isin C$, $D \le C$, OK for $C \zhaspatch \p$.

For $D \neq C$: $D \le C \equiv D \le L \lor D \le R
 \equiv D \isin L \lor D \isin R$.
(Likewise $D \le C \equiv D \le X \lor D \le Y$.)

Consider $D \neq C, D \isin X \land D \isin Y$:
By \commitmergename, $D \isin C$.  Also $D \le X$
so $D \le C$.  OK for $C \zhaspatch \p$.

Consider $D \neq C, D \not\isin X \land D \not\isin Y$:
By \commitmergename, $D \not\isin C$.
And $D \not\le X \land D \not\le Y$ so $D \not\le C$.
OK for $C \zhaspatch \p$.

Remaining case, wlog, is $D \not\isin X \land D \isin Y$.
$D \not\le X$ so $D \not\le M$ so $D \not\isin M$.
Thus by \commitmergename, $D \isin C$.  And $D \le Y$ so $D \le C$.
OK for $C \zhaspatch \p$.

So, in all cases, $C \zhaspatch \p$.
And by $L \haspatch \p$, $\exists_{F \in \py} F \le L$
and this $F \le C$ so indeed $C \haspatch \p$.

\subsubsection{For (wlog) $X \not\haspatch \p, Y \haspatch \p$:}

One of the Merge Ends conditions applies.
Recall that we are considering $D \in \py$.
$D \isin Y \equiv D \le Y$.  $D \not\isin X$.
We will show for each of
various cases that
if $M \haspatch \p$, $D \not\isin C$,
whereas if $M \nothaspatch \p$, $D \isin C \equiv D \le C$.
And by $Y \haspatch \p$, $\exists_{F \in \py} F \le Y$ and this
$F \le C$ so this suffices.

Consider $D = C$:  Thus $C \in \py, L \in \py$.
By Tip Own Contents, $L \haspatch \p$ so $L \neq X$,
therefore we must have $L=Y$, $R=X$.
Conversely $R \not\in \py$
so by Tip Merge $M = \baseof{L}$.  Thus $M \in \pn$ so
by Base Acyclic $M \nothaspatch \p$.  By \commitmergename, $D \isin C$,
and $D \le C$.  OK.

Consider $D \neq C, M \nothaspatch \p, D \isin Y$:
$D \le Y$ so $D \le C$.
$D \not\isin M$ so by \commitmergename, $D \isin C$.  OK.

Consider $D \neq C, M \nothaspatch \p, D \not\isin Y$:
$D \not\le Y$.  If $D \le X$ then
$D \in \pancsof{X}{\py}$, so by Addition Merge Ends and
Transitive Ancestors $D \le Y$ --- a contradiction, so $D \not\le X$.
Thus $D \not\le C$.  By \commitmergename, $D \not\isin C$.  OK.

Consider $D \neq C, M \haspatch \p, D \isin Y$:
$D \le Y$ so $D \in \pancsof{Y}{\py}$ so by Removal Merge Ends
and Transitive Ancestors $D \in \pancsof{M}{\py}$ so $D \le M$.
Thus $D \isin M$.  By \commitmergename, $D \not\isin C$.  OK.

Consider $D \neq C, M \haspatch \p, D \not\isin Y$:
By \commitmergename, $D \not\isin C$.  OK.

$\qed$

\subsection{Base Acyclic}

This applies when $C \in \pn$.
$C \in \pn$ when $L \in \pn$ so by Merge Acyclic, $R \nothaspatch \p$.

Consider some $D \in \py$.

By Base Acyclic of $L$, $D \not\isin L$.  By the above, $D \not\isin
R$.  And $D \neq C$.  So $D \not\isin C$.

$\qed$

\subsection{Tip Contents}

We need worry only about $C \in \py$.
And $\patchof{C} = \patchof{L}$
so $L \in \py$ so $L \haspatch \p$.  We will use the Unique Base
of $C$, and its Coherence and Patch Inclusion, as just proved.

Firstly we show $C \haspatch \p$: If $R \in \py$, then $R \haspatch
\p$ and by Coherence/Inclusion $C \haspatch \p$ .  If $R \not\in \py$
then by Tip Merge $M = \baseof{L}$ so by Base Acyclic and definition
of $\nothaspatch$, $M \nothaspatch \p$.  So by Coherence/Inclusion $C
\haspatch \p$ (whether $R \haspatch \p$ or $\nothaspatch$).

We will consider an arbitrary commit $D$
and prove the Exclusive Tip Contents form.

\subsubsection{For $D \in \py$:}
$C \haspatch \p$ so by definition of $\haspatch$, $D \isin C \equiv D
\le C$.  OK.

\subsubsection{For $D \not\in \py, R \not\in \py$:}

$D \neq C$.  By Tip Contents of $L$,
$D \isin L \equiv D \isin \baseof{L}$, so by Tip Merge condition,
$D \isin L \equiv D \isin M$.  So by \commitmergename, $D \isin
C \equiv D \isin R$.  And $R = \baseof{C}$ by Unique Base of $C$.
Thus $D \isin C \equiv D \isin \baseof{C}$.  OK.

\subsubsection{For $D \not\in \py, R \in \py$:}

$D \neq C$.

By Tip Contents
$D \isin L \equiv D \isin \baseof{L}$ and
$D \isin R \equiv D \isin \baseof{R}$.

Apply Tip Merge condition.
If $\baseof{L} = M$, trivially $D \isin M \equiv D \isin \baseof{L}.$
Whereas if $\baseof{L} = \baseof{M}$, by definition of $\base$,
$\patchof{M} = \patchof{L} = \py$, so by Tip Contents of $M$,
$D \isin M \equiv D \isin \baseof{M} \equiv D \isin \baseof{L}$.

So $D \isin M \equiv D \isin L$ so by \commitmergename,
$D \isin C \equiv D \isin R$.  But from Unique Base,
$\baseof{C} = \baseof{R}$.
Therefore $D \isin C \equiv D \isin \baseof{C}$.  OK.

$\qed$

\subsection{Unique Tips}

For $L \in \py$, trivially $\pendsof{C}{\py} = C$ so $T = C$ is
suitable.

For $L \not\in \py$, $\pancsof{C}{\py} = \pancsof{L}{\py} \cup
\pancsof{R}{\py}$.  So $T$ from Suitable Tips is a suitable $T$ for
Unique Tips.

$\qed$

\subsection{Foreign Inclusion}

Consider some $D \in \foreign$.
By Foreign Inclusion of $L, M, R$:
$D \isin L \equiv D \le L$;
$D \isin M \equiv D \le M$;
$D \isin R \equiv D \le R$.

\subsubsection{For $D = C$:}

$D \isin C$ and $D \le C$.  OK.

\subsubsection{For $D \neq C, D \isin M$:}

Thus $D \le M$ so $D \le L$ and $D \le R$ so $D \isin L$ and $D \isin
R$.  So by \commitmergename, $D \isin C$.  And $D \le C$.  OK.

\subsubsection{For $D \neq C, D \not\isin M, D \isin X$:}

By \commitmergename, $D \isin C$.
And $D \isin X$ means $D \le X$ so $D \le C$.
OK.

\subsubsection{For $D \neq C, D \not\isin M, D \not\isin L, D \not\isin R$:}

By \commitmergename, $D \not\isin C$.
And $D \not\le L, D \not\le R$ so $D \not\le C$.
OK

$\qed$

\subsection{Foreign Ancestry}

Only relevant if $\isforeign{L}$, in which case
$\isforeign{C}$ and by Foreign Merge $\isforeign{R}$,
so Totally Foreign Ancestry applies.  $\qed$

\subsection{Bases' Children}

If $L \in \py, R \in \py$: not applicable for either $D=L$ or $D=R$.

If $L \in \py, R \in \pn$: not applicable for $L$, OK for $R$.

Other possibilities for $L \in \py$ are excluded by Tip Merge.

If $L \in \pn, R \in \pn$: satisfied for both $L$ and $R$.

If $L \in \pn, R \in \foreign$: satisfied for $L$, not applicable for
$R$.

If $L \in \pn, R \in \pqy$: satisfied for $L$, not applicable for
$R$.

Other possibilities for $L \in \pn$ are excluded by Base Merge.

If $L \in \foreign$: not applicable for $L$; nor for $R$, by Foreign Merge.

$\qed$
