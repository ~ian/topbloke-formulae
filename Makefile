
SRCS := $(wildcard *.tex)

default: final.pdf final.ps

once:		revid.inc
		pdflatex -halt-on-error article.tex </dev/null

GITDIR:=$(addsuffix /, $(shell git rev-parse --git-dir))
HEADREF:=$(shell git symbolic-ref HEAD 2>/dev/null)
HEADDEPS:=$(addprefix $(GITDIR), HEAD $(HEADREF))

revid.inc:	.git-revid $(SRCS) Makefile $(HEADDEPS)
		./$< >$@.new
		cmp $@.new $@ || mv -f $@.new $@

final.pdf: $(SRCS) revid.inc
		$(MAKE) once
		$(MAKE) once
		$(MAKE) once
		cp article.pdf final.pdf

%.ps: %.pdf
		pdftops $*.pdf $*.ps

clean:
	rm -f *.aux *.log *.dvi *.out *.pdf *.ps *~

# psnup -pa4 -2 <final.ps | ssh davenant cat junk/tumble.ps - \| lpr
